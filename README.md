<!-- PROJECT LOGO -->
<br />
<p align="center">
  <!-- <a href="https://github.com/Krakcen/hugoworld-discord">
    <img src="images/logo.png" alt="Logo" width="80" height="80">
  </a> -->

  <h3 align="center">hugoworld-discord</h3>

  <p align="center">
    Provides knowledge
    <br />
  </p>
</p>



<!-- TABLE OF CONTENTS -->
<details open="open">
  <summary><h2 style="display: inline-block">Table of Contents</h2></summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>
    <li><a href="#usage">Usage</a></li>
    <li><a href="#license">License</a></li>
    <li><a href="#contact">Contact</a></li>
  </ol>
</details>



<!-- ABOUT THE PROJECT -->
## About The Project

Discord 

### Built With

* [nodejs](https://nodejs.org/en/)
* [express](https://expressjs.com/)
* [discord.js](https://github.com/discordjs/discord.js#installation)



<!-- GETTING STARTED -->
## Getting Started

### Prerequisites

You will need a discord application and bot, to go to the developer portal, go [here](https://discord.com/developers/applications).
Once you get your keys, create a `.env` from `.env.dist` and replace the default values

```sh
> cp .env.dist .env
```

This project run on nodejs, make sure you are using the same version as the one specified in `package.json`
<br/>
I recommend you to use [nvm](https://github.com/nvm-sh/nvm) to manage node versions


### Installation

```sh
> npm install
```

<!-- USAGE EXAMPLES -->
## Usage

```sh
> npm start
```

<!-- LICENSE -->
## License

Distributed under the MIT License. See `LICENSE` for more information.



<!-- CONTACT -->
## Contact

Hugo - hugo.villevieille@epitech.eu

Project Link: [hugoworld-discord](https://gitlab.com/Krakcen/hugoworld-discord)
