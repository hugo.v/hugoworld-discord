const express = require('express');
const path = require('path');
const App = require('./app');
const logger = require('./logger');

const PORT = Number(process.env.WEB_PORT || 3000);

const app = App();

const frontPath = `${__dirname}/../interface`;

app.use(express.static(path.join(frontPath, 'build')));

// Bot Admin Interface
app.get('/', (req, res) => {
  res.sendFile(path.join(frontPath, 'build', 'index.html'));
});

app.listen(PORT, () => {
  logger.info(`Example app listening at http://localhost:${PORT}`);
});
