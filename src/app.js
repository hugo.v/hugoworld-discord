require('dotenv').config();

const express = require('express');
const Discord = require('discord.js');

const commands = require('./commands');
const cron = require('./cron');
const discordIds = require('./constants/discord-ids');

module.exports = () => {
  const app = express();

  const client = new Discord.Client();
  client.commands = new Discord.Collection();

  commands.forEach((command) => {
    client.commands.set(command.name, command);
  });

  client.once('ready', async () => {
    cron.gouter(client, discordIds.LULZ);
  });

  client.on('message', (message) => {
    const PREFIX = '!';
    if (!message.content.startsWith(PREFIX) || message.author.bot) return;

    const args = message.content.slice(PREFIX.length).trim().split(/ +/);
    const commandName = args.shift().toLowerCase();

    if (!client.commands.has(commandName)) return;

    const command = client.commands.get(commandName);
    if (command.args && !args.length) {
      message.channel.send(`You didn't provide any arguments, ${message.author}!`);
    }

    try {
      command.execute(message, args);
    } catch (error) {
      console.error(error);
      message.reply('there was an error trying to execute that command!');
    }
  });

  client.login(process.env.DISCORD_BOT_TOKEN);
  return app;
};
