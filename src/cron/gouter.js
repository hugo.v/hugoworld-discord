const cron = require('node-cron');
const axios = require('axios');
const Discord = require('discord.js');

const logger = require('../logger');

const optionsSearchListOfGouters = {
  method: 'GET',
  url: 'https://www.themealdb.com/api/json/v1/1/filter.php?c=Dessert',
  params: {

  },
  headers: {
  },
};

const optionsSearchGouter = (gouterId) => ({
  method: 'GET',
  url: `https://www.themealdb.com/api/json/v1/1/lookup.php?i=${gouterId}`,
  params: {

  },
  headers: {
  },
});

const gouter = async (client, userId) => {
  try {
    const goutersResp = await axios.request(optionsSearchListOfGouters);
    const goutersList = goutersResp.data.meals;

    cron.schedule('0 16 * * *', async () => {
      const randomGouter = goutersList[Math.floor(Math.random() * goutersList.length)];
      const { data: gouterData } = await axios.request(optionsSearchGouter(randomGouter.idMeal));
      const {
        strMeal, strYoutube, strMealThumb, strSource,
      } = gouterData.meals[0];

      const gouterMessage = new Discord.MessageEmbed()
        .setTitle(strMeal)
        .setURL(strYoutube)
        .setAuthor("🍫 Guillaume, c'est l'heure du goûter ! 🍪")
        .setDescription("Je t'ai dégoté une recette OKLM, check ça mon soss:")
        .setThumbnail(strMealThumb)
        .setFooter(strSource);

      const user = await client.users.fetch(userId);
      user.send(gouterMessage);
      logger.info('[CRON] - gouter invoked');
    });
  } catch (error) {
    console.log('ERROR IN GOUTERS SERVICE: ', error);
  }
};

module.exports = gouter;
