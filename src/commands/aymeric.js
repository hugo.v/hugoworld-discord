const Discord = require('discord.js');
const logger = require('../logger');

module.exports = {
  name: 'aymeric',
  args: false,
  description: 'Qui suis je ?',
  execute(message, args) {
    const aymericBio = new Discord.MessageEmbed()
      .setColor('#0099ff')
      .setTitle('Commande Aymeric')
      .setURL('https://discord.js.org/')
      .setAuthor('Aymeric', 'https://cdn.discordapp.com/avatars/237172495562702848/67f9987f5d09adf448752219303ae3b1.png?size=256')
      .setDescription("Salut c'est Aymeric")
      .setThumbnail('https://cdn.discordapp.com/avatars/237172495562702848/67f9987f5d09adf448752219303ae3b1.png?size=256')
      .addFields(
        { name: 'Age', value: '20' },
      )
      .setImage('https://cdn.discordapp.com/avatars/237172495562702848/67f9987f5d09adf448752219303ae3b1.png?size=256')
      .setTimestamp()
      .setFooter("Merci d'avoir check mon profil", 'https://cdn.discordapp.com/avatars/237172495562702848/67f9987f5d09adf448752219303ae3b1.png?size=256');
    message.channel.send(aymericBio);
    logger.info('[COMMAND] - aymeric invoked');
  },
};
